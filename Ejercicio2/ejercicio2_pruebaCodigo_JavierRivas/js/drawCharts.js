//Autor Fco Javier Rivas Torres © 2017-2018
//Script que pinta en un grafico de tarta y de lineas el valor de tres set de datos


// Pedimos el valor de la primera seria y cuando nos haya llegado bien continuamos pidiendo el resto de datos
getSerie1();


function getSerie1(){
    $.ajax({
        url: "http://s3.amazonaws.com/logtrust-static/test/test/data1.json",
        type: "GET",
        dataType: "json",
        data: {},
        // Pedimos el primer set de datos, podemos usar el pruebas que tiene menos datos para comprobar que hemos calculado bien todo
        success: function(data){
            //var prueba = [{"d":1435708800000,"cat":"Cat 1","value":832.803815816826},{"d":1435708800000,"cat":"Cat 1","value":832.803815816826},{"d":1435708800000,"cat":"Cat 1","value":935.6381772249989},{"d":1435968000000,"cat":"Cat 1","value":54.70066995525236}];
            var formalizeValue = formalizeValues(data); // Formalizamos los valores, valores no claves, es decir fecha en YYYY-MM-DD y categoria en mayusculas
            var dataFormalize1 = formalizeSerie1(formalizeValue); // Formalizamos las claves para poder mergear los set de datos correctamente
            dataMergeSerie1 = mergeSerie(dataFormalize1); // Mergeamos los datos para unir las categorias y las fechas coincidentes
            console.log("serie 1: ",JSON.stringify(dataMergeSerie1));
            getSerie2(dataMergeSerie1); // Pedimos el set de datos 2
        },
        error: function(error){
             console.log("Error:");
             console.log(error);
        }
    });
}

function getSerie2(serie1){
    $.ajax({
        url: "http://s3.amazonaws.com/logtrust-static/test/test/data2.json",
        type: "GET",
        dataType: "json",
        data: {},
        success: function(data){
            //var prueba = [{"myDate":"2015-07-01","categ":"CAT 1","val":46.300059172697175},{"myDate":"2015-06-02","categ":"CAT 1","val":92.41149931632995},{"myDate":"2015-06-19","categ":"CAT 3","val":11.023899406944393}];
            // No hace falta formalizar los valores
            var dataFormalize = formalizeSerie2(data); // Formalizamos las claves para poder mergear los set de datos correctamente
            var dataMergeSerie2 = mergeSerie(dataFormalize); // Mergeamos los datos para unir las categorias y las fechas coincidentes
            console.log("serie 2: ",JSON.stringify(dataMergeSerie2));
            getSerie3(serie1,dataMergeSerie2); // Pedimos el set de datos 3
        },
        error: function(error){
             console.log("Error:");
             console.log(error);
        }
    });
}

function getSerie3(serie1,serie2){
    $.ajax({
        url: "http://s3.amazonaws.com/logtrust-static/test/test/data3.json",
        type: "GET",
        dataType: "json",
        data: {},
        success: function(data){
            //var prueba = [{"raw":"9OHbc9 O1 WHTxiBPa auwZIVD6 j8jMWWVH UdB6hy 2015-06-18 XF 5xhcx15DD sbYFRPn dyoH1OOIF 6meHw pANknwa2h T imhs24gR5 #CAT 1#","val":39.38690127513058},{"raw":"YCcoeoNR8 T4VSBd0GC fpAepuTD 5A40zJ6 y5bXBb rRxM 2015-06-08 J KA9FicdV BSbvirf #CAT 2#","val":74.10101967551246},{"raw":"thJP4b 2015-06-26 bDes w7iyahba RZ8ycJ55Q #CAT 2#","val":66.76803037123229},{"raw":"nX tSP ABv2AG 2015-06-12 6 Vwb #CAT 2#","val":53.992466763210835},{"raw":"0J4ug jIyQXydtv 2015-06-01 jFbHT9egK7 dXa5uDkZ WU BusedKDK0 rf UcuF0AmFw DgCjaMqc ca1JgBk TI E5tEq6 #CAT 2#","val":23.395346314392796},{"raw":"DAiW7 2015-06-06 zcJU zbXa O FuAM H38XE3B WesRrygD 3I8APsO2i #CAT 1#","val":63.8569758442417},{"raw":"tyMNp sq9lX97aPg 2015-06-21 fQox5DFL KM5mH8pS8w YEOsPD C iNaOc4o5Fi MmtT HUYQHzMt #CAT 1#","val":87.08892087437587},{"raw":"DOGdO 4FDVQcBE b8g0LxpZ0 BjUUv W2W8P5QX lKIYW 2015-06-26 SCU 3 C WrW IogYKTg XCXDF Y Z q9sc #CAT 1#","val":53.65045284124685}];
            var dataSerie3 = restructureSerie3(data);
            var dataMergeSerie3 = mergeSerie(dataSerie3);

            console.log("serie 3: ",JSON.stringify(dataMergeSerie3));
            var obj = mergeAllSeries(serie1, serie2, dataMergeSerie3);
            console.log("seriesMerged: ",JSON.stringify(obj));
            var dataFinalLineChart = dataLineChart(obj);
            var dataFinalPieChart = dataPieChart(obj);
            drawPieChart(dataFinalPieChart);
            var lineChartSeries = buildSeriesLineChart(dataFinalLineChart);
            drawLineChart(lineChartSeries);
        },
        error: function(error){
             console.log("Error:");
             console.log(error);
        }
    });
}

// Funcion que mezcla las tres series en un solo set de datos, no podemos usar funciones de libreria
// ya que queremos duplicidad, nos sirven todos los datos, no solo el ultimo de la serie repetida
function mergeAllSeries(){

    var output  = [];
    var temp    = [];
    var keys    = [];
    for(var i = 0; i < arguments.length; i++){ 
        var obj = arguments[i];
        keys.push(Object.keys(obj));
        var aux = _.merge()    
        output.push(obj);
    }
    return output;
}

// Funcion que formaliza los valores de la serie uno, fecha y categoria
function formalizeValues(data){

    data.forEach(function(item){
        item.d = new Date(item.d).toISOString().substr(0, 10);
        item.cat = item.cat.toUpperCase();
    });
    return data;
}

// Funcion que formaliza las claves de la serie 1
function formalizeSerie1(serie1){
    var keyMap = {
        cat: 'cat',
        d: 'date',
        value: 'value'
    };
    return renameKeys(serie1,keyMap);
}

// Funcion que formaliza las claves de la serie 2
function formalizeSerie2(serie2){
    var keyMap = {
        categ: 'cat',
        myDate: 'date',
        val: 'value'
    };
    return renameKeys(serie2,keyMap);    
}

// Funcion que una vez contruidas las nuevas clave valor renombra los set de datos
function renameKeys(serie,keyMap){

    return serie.map(function(obj) {
        return _.mapKeys(obj, function(value, key) {
            return keyMap[key];
        });
    });
}

// Funcion que une para las mismas categorias y fechas los valores, cambia las calves,
// ahora pasaran a ser dinamicas, no sabremos cuantas categorias ni fechas tendremos, esto hara que el trabajo sea mas artesanal
function mergeSerie (data) {
    return _.chain(data).groupBy('cat').mapValues(function (v) {
        return _.chain(v).groupBy('date').mapValues(function (x) {
            return _.chain(x).map('value').flattenDeep().value();
         }).value();  
        return v;
    }).value();
}

// Funcion que monta de manera artesanal los datos para el pie chart, ya que no conocemos las claves de las categorias
// en este caso las fechas nos dan igual, ya que es un sumatorio
// vamos montando los objetos manualmente y uniendolos
function dataPieChart(data) {

    var keys = [];

    var dataPieChart = [];
    var res = 0;
    
    data.forEach(function(item){

        _.mapKeys(item, function(value, key) {
            keys.push(key);  
        });    
    });
    keys = keys.filter(function(elem, index, self) {
        return index == self.indexOf(elem);
    })
    // Recorremos todas las claves no duplicadas
    keys.forEach(function(key){
        var catObjPie = {
            'name':'',
            'y':''
        };
        catObjPie.name = key;
        // Por cada clave recorremos el set sumando los valores totales
        data.forEach(function(item){
            _.mapValues(item[key], function(values, keyDate) {
                values.forEach(function(value){                  
                    res = res + value;
                }); 
            }); 
        });
        catObjPie.y = res;
        res = 0;
        dataPieChart.push(catObjPie);
           
    });

    return dataPieChart;
}

// Funcion que monta los datos del grafico de lineas de manera artesanal, ya que no conoemos las claves
// de las categorias ni de las fechas que en este casi si nos interesa, ya que debemos unir los datos de las fechas por categoria iguales
// vamos montando los objetos manualmente y uniendolos
function dataLineChart(data) {

    var keys = [];
    var dataKeys = [];

    var dataLineChart = [];
    var res = 0;
    
    /*Al no conocer nuestras keys debemos recoger todas las que haya
    tanto de categoria como de fecha para luego montar nuestro set de datos*/
    data.forEach(function(item){
        //console.log(item);
        _.mapKeys(item, function(value, key) {
            keys.push(key);
            _.mapValues(item[key], function(values, keyDate) {
                dataKeys.push(keyDate);
            }); 
        });    
    });
    // Evitamos claves duplicadas
    keys = keys.filter(function(elem, index, self) {
        return index == self.indexOf(elem);
    });
    // Evitamos fechas duplicadas
    dataKeys = dataKeys.filter(function(elem, index, self) {
        return index == self.indexOf(elem);
    });
    // Ordenamos las fechas
    dataKeys.sort(function(a,b){
        var da = new Date(a).getTime();
        var db = new Date(b).getTime();
  
        return da < db ? -1 : da > db ? 1 : 0
    });

    // Recorremos todas nuestras claves de categoria
    keys.forEach(function(key){
        var catObjLine = {
            'categoria':'',
            'fechas':[]
        };
        // Recorremos nuestras claves de fecha
        catObjLine.categoria = key;
        dataKeys.forEach(function(dataKey){
            var find = false;
            var dateObj = {
                'fecha':'',
                'totalValue':''
            }
            // Recorremos todo nuestro data set mezclando por claves categoria y fecha el sumatorio de los valores
            data.forEach(function(item){
                _.mapValues(item[key], function(values, keyDate) {
                    if (keyDate === dataKey){
                       find = true;
                       values.forEach(function(value){                  
                            res = res + value;
                        });  
                    }                 
                }); 
            });
            if ( find ){
                dateObj.fecha = dataKey;
                dateObj.totalValue = res;
                catObjLine.fechas.push(dateObj);
                res = 0;
            }
        });
        dataLineChart.push(catObjLine);        
    });

    return dataLineChart;
}

// Funcion auxiliar que nos ayuda a contruir las series del line chart
function buildSeriesLineChart(dataLineChart){

    var series = [];
    
    dataLineChart.forEach(function(cat){
        var serie = {
            type: 'area',
            name: '',
            data: []
        };
        serie.name = cat.categoria;
        cat.fechas.forEach(function(fecha){
            serie.data.push([Date.parse(fecha.fecha),fecha.totalValue]);
        });
        series.push(serie);
    });

    return series;    

}

// Function que reestructura el set de datos 3 para recoger la fecha y la categoria
// aplicamos expresiones regulares
function restructureSerie3 (dataS3) {

    var serie3 = [];
    
    var regExDate = /\d{4}-\d{2}-\d{2}/;
    var regExCat = /\#.+\#/;

    dataS3.forEach(function(item){

        var serie = {
            'date':'',
            'cat':'',
            'value':''
        };

        var date = item.raw.match(regExDate)[0];
        var cat = item.raw.match(regExCat)[0].replace("#",'').replace("#",'');

        serie.date = date;
        serie.cat = cat;
        serie.value = item.val;

        serie3.push(serie);

    });

    return serie3;

}

// Funcion que pinta el grafico de tarta
function drawPieChart(data){
    Highcharts.chart('containerPieChart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Valor total por categorias'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Categorias',
            colorByPoint: true,
            data: data
        }]
    });
}

// Funcion que pinta el grafico de lineas
function drawLineChart(series){
            Highcharts.chart('containerLineChart', {
                chart: {
                    zoomType: 'x'
                },
                title: {
                    text: 'Categorias por intervalos irregulares de tiempo'
                },
                subtitle: {
                    text: document.ontouchstart === undefined ?
                            'Haz click y arrastra para hacer zoom' : 'Pinch the chart to zoom in'
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Valor'
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                //[0, Highcharts.getOptions().colors[0]],
                                //[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        },
                        marker: {
                            radius: 2
                        },
                        lineWidth: 1,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        },
                        threshold: null
                    }
                },

                series: series
            });
        
}
