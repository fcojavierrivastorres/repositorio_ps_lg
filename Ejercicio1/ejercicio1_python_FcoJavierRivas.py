"""
@author: {Fco Javier Rivas Torres aka FJrivax}
@title: {Ejercicio 1}
@version: 0.1
@date: 10-08-2017
"""
# Programa en Python que calcula si un numero de una lista es:
# * perfecto ( suma de sus divisores menos el es el mismo )
# * abundante ( suma de sus divisores menos el es superior )
# * deficiente ( suma de sus divisores menos el es inferior )

lista = [6,7,8,9,10,11,12]
divisorIni = 1

# Tenemos dos maneras de realizar el proceso, con un bucle for que nos vaya dando los divisores
# o con recursividad, evitando el propio bucle ( opcion mas optima )

# Bucle FOR
# vamos recorriendo el numero -1 y comprobamos que el resto es 0, es decir divisor, bingo
def unDivisor(number):
	
    return [i for i in range(1, number) if number % i == 0]

# RECURSIVIDAD ( esta nos gusta mas, con el handicap de que es menos legible :( )
# Siempre que nuestro divisor sea menor que el numero y el resto de la division del numero con el divisor sea 0, sumanos
# el divisor mas el resultado que devuelva la siguiente ejecucion de la funcion recursiva
# Si no es divisible el divisor simplemente llamamos a la funcion de nuevo con los nuevos valores sin sumar nada
# Por ultimo si terminamos las condiciones devolvemos un 0 ya que no queremos que se sume el propio numero a nuestro resultado
def recurSumDivisores(number,divisor):
   
   if (divisor < number-1) & (number % divisor == 0): return divisor + recurSumDivisores(number,divisor+1)
   else:  	
   	   if (divisor < number-1): return recurSumDivisores(number,divisor+1)  
   	   else: return 0


# Clasificamos cada numero, dependiendo de que manera le indiquemos en la funcion general e imprimimos el resultado
def clasificador(number,recursividad):

	sumaDivisores = 0;

	if (recursividad): sumaDivisores = recurSumDivisores(number,divisorIni)
	else: sumaDivisores = sum(unDivisor(number))	

	if (sumaDivisores == 0):print("No pudimos calcular el tipo del numero",number)
	elif (sumaDivisores > number):print("Es un numero abundante",number)
	elif (sumaDivisores < number):print("Es un numero deficiente",number)
	elif (sumaDivisores == number):print("Es un numero perfecto",number)
    	
# Por ultimo tenemos la ejecucion total del programa donde recorremos la lista y le indicamos de que forma quiere
# que le calculemos el resultado, sin con un bucle o con recursividad

def listaClasificada():
	
	for unElemento in lista:
		clasificador(unElemento,True)


listaClasificada()   